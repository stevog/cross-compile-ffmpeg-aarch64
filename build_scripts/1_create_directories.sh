#!/usr/bin/env bash

script_path="$( cd "$(dirname "$0")" ; pwd -P )"

source ${script_path}/0_export_variables.sh

mkdir -p ${FFMPEG_SOURCES} ${LIBAOM_SOURCES} ${MPP_SOURCES} ${FFMPEG_BIN} ${LIBAOM_BIN} ${FFMPEG_BUILD} ${LIBAOM_BUILD} ${PKG_CONFIG_PATH}
