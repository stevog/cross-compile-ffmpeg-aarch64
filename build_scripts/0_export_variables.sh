#!/usr/bin/env bash

export FFMPEG_SOURCES="/home/builder/build/ffmpeg/ffmpeg_sources"
export LIBAOM_SOURCES="/home/builder/build/ffmpeg/libaom_sources"
export MPP_SOURCES="/home/builder/build/ffmpeg/mpp_sources"
export FFMPEG_BIN="/home/builder/build/ffmpeg/ffmpeg_bin"
export LIBAOM_BIN="/home/builder/build//libaom_bin"
export FFMPEG_BUILD="/home/builder/build//ffmpeg_build"
export LIBAOM_BUILD="/home/builder/build/libaom_build"
export PKG_CONFIG_PATH="/home/builder/build/lib/pkgconfig"
export ARCH=arm64
