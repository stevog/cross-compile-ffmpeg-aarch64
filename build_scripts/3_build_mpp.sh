#!/usr/bin/env bash

script_path="$( cd "$(dirname "$0")" ; pwd -P )"

source ${script_path}/0_export_variables.sh

export BUILD_START_TIME=$(date +%d-%m-%Y_%H:%M:%S) && \
cd ${MPP_SOURCES} && \
wget https://github.com/rockchip-linux/mpp/archive/release.zip && \
unzip release.zip && \
cd mpp-release && \
cmake -DRKPLATFORM=ON -DHAVE_DRM=ON -DAOM_TARGET_CPU="arm64" && make >${MPP_SOURCES}/log-cmake-${BUILD_START_TIME}.txt 2>&1

error_code=$?
if [[ ${error_code} != 0 ]]; then
    echo "cmake for mpp failed"
    exit ${error_code}
fi

cp rockchip_mpp.pc rockchip_vpu.pc ${PKG_CONFIG_PATH}
