#!/usr/bin/env bash

script_path="$( cd "$(dirname "$0")" ; pwd -P )"
log_file="/home/builder/build/execute_script.log"

source ${script_path}/0_export_variables.sh

echo 0 > ${log_file}

cd ${script_path}
./1_create_directories.sh
error_code=$?
if [[ ${error_code} != 0 ]]; then
    echo "1_create_directories.sh failed" > ${log_file}
    exit ${error_code}
fi

cd ${script_path}
./2_build_libaom.sh
error_code=$?
if [[ ${error_code} != 0 ]]; then
    echo "2_build_libaom.sh failed" > ${log_file}
    exit ${error_code}
fi

cd ${script_path}
./3_build_mpp.sh
error_code=$?
if [[ ${error_code} != 0 ]]; then
    echo "3_build_mpp.sh failed" > ${log_file}
    exit ${error_code}
fi

cd ${script_path}
./4_build_ffmpeg.sh
error_code=$?
if [[ ${error_code} != 0 ]]; then
    echo "4_build_ffmpeg.sh failed" > ${log_file}
    exit ${error_code}
fi

echo 0 > ${log_file}
exit 0