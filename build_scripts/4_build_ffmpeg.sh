#!/usr/bin/env bash

script_path="$( cd "$(dirname "$0")" ; pwd -P )"

source ${script_path}/0_export_variables.sh

export BUILD_START_TIME=$(date +%d-%m-%Y_%H:%M:%S) && \
cd ${FFMPEG_SOURCES} && \
wget -O ffmpeg-snapshot.tar.bz2 https://ffmpeg.org/releases/ffmpeg-snapshot.tar.bz2 && \
tar xjvf ffmpeg-snapshot.tar.bz2 && \
cd ffmpeg && \
PATH="$FFMPEG_BIN:$PATH" PKG_CONFIG_PATH=${PKG_CONFIG_PATH} CC=aarch64-linux-gnu-gcc ./configure \
  --prefix="$FFMPEG_BUILD" \
  --pkg-config-flags="--static" \
  --extra-cflags="-I$FFMPEG_BUILD/include" \
  --extra-ldflags="-L$FFMPEG_BUILD/lib" \
  --extra-libs="-lpthread -lm" \
  --bindir="$FFMPEG_BIN" \
  --enable-gpl \
  --enable-libaom \
  --enable-libass \
  --enable-libfdk-aac \
  --enable-libfreetype \
  --enable-libmp3lame \
  --enable-libopus \
  --enable-libvorbis \
  --enable-libvpx \
  --enable-libx264 \
  --enable-rkmpp \
  --enable-version3 \
  --enable-libdrm \
  --enable-libx265 \
  --enable-nonfree && \
PATH="$FFMPEG_BIN:$PATH" make >${FFMPEG_SOURCES}/log-make-${BUILD_START_TIME}.txt 2>&1

error_code=$?
if [[ ${error_code} != 0 ]]; then
    echo "make for ffmpeg failed"
    exit ${error_code}
fi

export BUILD_START_TIME=$(date +%d-%m-%Y_%H:%M:%S) && \
make install >${FFMPEG_SOURCES}/log-make-install-${BUILD_START_TIME}.txt 2>&1

error_code=$?
if [[ ${error_code} != 0 ]]; then
    echo "make install for ffmpeg failed"
    exit ${error_code}
fi
