#!/usr/bin/env bash

script_path="$( cd "$(dirname "$0")" ; pwd -P )"

source ${script_path}/0_export_variables.sh

#cmake Option falls nötig: -DAOM_TARGET_CPU="arm64"
export BUILD_START_TIME=$(date +%d-%m-%Y_%H:%M:%S) && \
cd ${LIBAOM_SOURCES} && \
git -C aom pull 2> /dev/null || git clone --depth 1 https://aomedia.googlesource.com/aom && \
mkdir -p aom_build && \
cd aom_build && \
PATH="$LIBAOM_BIN:$PATH" cmake -G "Unix Makefiles" -DCMAKE_INSTALL_PREFIX="$LIBAOM_BUILD" -DENABLE_SHARED=off -DENABLE_NASM=on \
        ../aom >${LIBAOM_SOURCES}/log-cmake-${BUILD_START_TIME}.txt 2>&1

error_code=$?
if [[ ${error_code} != 0 ]]; then
    echo "cmake for aom failed"
    exit ${error_code}
fi

export BUILD_START_TIME=$(date +%d-%m-%Y_%H:%M:%S) && \
PATH="$LIBAOM_BIN:$PATH" make >${LIBAOM_SOURCES}/log-make-${BUILD_START_TIME}.txt 2>&1

error_code=$?
if [[ ${error_code} != 0 ]]; then
    echo "make for aom failed"
    exit ${error_code}
fi

cd ${LIBAOM_SOURCES} && \
export BUILD_START_TIME=$(date +%d-%m-%Y_%H:%M:%S) && \
make install >${LIBAOM_SOURCES}/log-make-install-${BUILD_START_TIME}.txt 2>&1

error_code=$?
if [[ ${error_code} != 0 ]]; then
    echo "make install for aom failed"
    exit ${error_code}
fi

cp ${LIBAOM_SOURCES}/aom_build/aom.pc ${PKG_CONFIG_PATH}
