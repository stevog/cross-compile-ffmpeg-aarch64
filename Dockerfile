FROM debian:stretch-slim

VOLUME build

ADD "packages/nasm_2.14-1_amd64.deb" /
ADD "packages/yasm_1.3.0-2+b1_amd64.deb" /

RUN sed -i 's/main/main\ non-free/g' /etc/apt/sources.list && \
                                 dpkg -i /nasm_2.14-1_amd64.deb && \
                                 dpkg -i /yasm_1.3.0-2+b1_amd64.deb && \
                                 apt update && apt install -y autoconf automake bash build-essential ccache cmake debhelper fakeroot git-core \
                                 libass-dev libfreetype6-dev libsdl2-dev libtool libva-dev \
                                 libvdpau-dev libvorbis-dev libxcb1-dev libxcb-shm0-dev libxcb-xfixes0-dev pkg-config texinfo unzip wget zlib1g-dev \
                                 libx264-dev libx265-dev libvpx-dev libfdk-aac-dev libmp3lame-dev libopus-dev gcc-aarch64-linux-gnu binutils-aarch64-linux-gnu && \
                                 rm -rf /var/lib/apt/lists/* && \
                                 /usr/sbin/update-ccache-symlinks && \
                                 useradd -r -u 1000 -d /home/builder -s /bin/bash -g users builder && \
                                 mkdir -p /home/builder/build/ccache && chown -R builder:users /home/builder

USER builder
WORKDIR /home/builder/build
RUN echo 'export PATH="/usr/lib/ccache:$PATH"' >> /home/builder/.bashrc
ADD config/ccache.conf /home/builder/.ccache/ccache.conf
ADD build_scripts/ /home/builder/build_scripts/

ENTRYPOINT ["bash", "/home/builder/build_scripts/execute_scripts.sh"]
