# cross-compile-ffmpeg-aarch64

Docker Image and Scripts to build the dependencies of ffmpeg and ffmpeg itself from another architecture for aarch64

## Build ffmpeg

Build and run the docker image. Everything will be executed automatically.

    ./build_docker.sh
    ./run-docker.sh

## Logs

The logs will be placed in each of the directories of ffmpeg and the needed libs, which are also build.

Example:

    tail -f build/ffmpeg/libaom_sources/log-cmake-08-02-2019_17:08:28.txt

The [execute_script](./build_scripts/execute_scripts.sh) also logs, in which build it fails.

    cat build/execute_script.log

## Issues

For now "aom.pc" is not found by pkg-config, so the build of ffmpeg aborts.
Maybe the other libs must also be built with an aarch64 target.