#!/usr/bin/env bash

script_path="$( cd "$(dirname "$0")" ; pwd -P )"
container_name=hardcore_benz
image_name=gcc-local-debian-sid-slim:latest

docker stop ${container_name}
docker rm ${container_name}
docker run -d --name ${container_name} -v ${script_path}/build:/home/builder/build ${image_name}
docker logs -f ${container_name}
